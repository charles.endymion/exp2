# Pyodide

On peut avoir un IDE avec un fichier Python préchargé.

Avec un fichier `exemple.py` qui se trouve dans le même dossier que votre `page.md` à créer.

On ajoute une ligne avec `{{ "{{ IDE('exemple') }}" }}`

{{ IDE('exemple') }}

On peut avoir un IDE avec un fichier Python préchargé.

Avec un fichier `exemple2.py` qui se trouve dans le même dossier que votre `page.md` à créer.

On ajoute une ligne avec `{{ "{{ IDE('exemple2') }}" }}`

{{ IDE('exemple2') }}

Pour de plus amples informations, le travail de Vincent Bouillot est présenté ici : <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>
